from flask import Flask, make_response
from flask import request
from flask import jsonify
from flaskext.mysql import MySQL
import jwt 
import datetime
from functools import wraps

app = Flask(__name__)

app.config['SECRET_KEY'] = 'clavesecreta'

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'pflutter'
app.config['MYSQL_DATABASE_PASSWORD'] = '12345678'
app.config['MYSQL_DATABASE_DB'] = 'passwords'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

conn = mysql.connect()
cursor =conn.cursor()


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('token')
        if not token:
            return jsonify({'message' : 'Token is missing!'}), 403

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'],algorithms="HS256");
        except:
            return jsonify({'message' : 'Token is invalid!'}), 403

        return f(*args, **kwargs)
    return decorated

@app.route('/login', methods=['POST'])
def login():
    params = {
        'username' : request.json['username'],
        'password' : request.json['password']
    }
    cursor.execute("SELECT * from accounts_token")
    rv = cursor.fetchall()

    for result in rv:
        if (result[0]==params['username'] and result[1]==params['password']):
            token = jwt.encode({'user' : params['username'], 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=60)}, app.config['SECRET_KEY'])
            return jsonify({'token' : token.decode('UTF-8')})

    return jsonify({'message' : 'Account incorrect.'})


@app.route('/register', methods=['POST'])
def register():
    params = {
        'username' : request.json['username'],
        'password' : request.json['password']
    }
    query = """insert into accounts_token (username,password)
            values (%(username)s, %(password)s)"""
    cursor.execute(query, params)
    conn.commit()
    return jsonify(params)

@app.route('/create_account', methods=['POST'])
@token_required
def create_account():
    params = {
        'account' : request.json['account'],
        'password' : request.json['password'],
        'platform' : request.json['platform'],
        'note' : request.json['note']
    }
    query = """insert into accounts (account, password, platform, note)
         values (%(account)s, %(password)s, %(platform)s, %(note)s)"""
    cursor.execute(query, params)
    conn.commit()

    content = {'id': cursor.lastrowid,
               'account': params['account'],
               'password': params['password'],
               'platform': params['platform'],
               'note': params['note']
               }
    return jsonify(content)

@app.route('/', methods=['GET'])
@token_required
def accounts():
    cursor.execute("SELECT * from accounts")
    #data = cursor.fetchone() # obtiene un registro
    rv = cursor.fetchall()
    data = []
    content = {}
    for result in rv:
        content = {'id': result[0],
                   'account': result[1],
                   'password': result[2],
                   'platform': result[3],
                   'note': result[4]
                   }
        data.append(content)
        content = {}
    return jsonify(data)
@app.route('/update_account', methods=['PUT'])
@token_required
def update_account():
    params = {
        'id' : request.json['id'],
        'account' : request.json['account'],
        'password' : request.json['password'],
        'platform' : request.json['platform'],
        'note' : request.json['note']
    }
    query = """update accounts set
               account = %(account)s,
               password = %(password)s,
               platform = %(platform)s,
               note = %(note)s
               where id = %(id)s
            """
    cursor.execute(query, params)
    conn.commit()

    content = {'id': params['id'],
               'account': params['account'],
               'password': params['password'],
               'platform': params['platform'],
               'note': params['note']
               }
    return jsonify(content)

@app.route('/delete_account', methods = ['DELETE'])
@token_required
def delete_account():
    params = {'id' : request.json['id']}
    query = """delete from accounts where id = %(id)s"""
    try:
        cursor.execute(query, params)
        conn.commit()
        return jsonify({'message' : 'Se elimino correctamente'})
    except:
        return jsonify({'message' : 'Ocurrio un error'})

if __name__ == "__main__":
    app.run(debug=True)