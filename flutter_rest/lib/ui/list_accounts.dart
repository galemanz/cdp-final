import 'dart:async';
import 'dart:convert';

import 'package:flutter_rest/ui/create_account.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class Cuenta {
  final String account;
  final int id;
  final String note;
  final String password;
  final String platform;

  Cuenta({this.account, this.id, this.note, this.password, this.platform});

  factory Cuenta.fromJson(Map<String, dynamic> json) {
    return Cuenta(
      account: json['account'],
      id: json['id'],
      note: json['note'],
      password: json['password'],
      platform: json['platform'],
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            backgroundColor: Colors.lightBlue,
            onPressed: () => _createNewClient(context),
          ),
          body: JsonListView(),
        ),
      );
  }
}

void _createNewClient(BuildContext context) async {
  await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CuentaScreen(Cuenta())),
  );
}

void _editClient(BuildContext context, Cuenta cuenta) async {
  await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CuentaScreen(cuenta)),
  );
}

class JsonListView extends StatefulWidget {
  JsonListViewWidget createState() => JsonListViewWidget();
}

class JsonListViewWidget extends State<JsonListView> {

    Future<Null> refreshList () async{
    await Future.delayed(Duration(seconds: 2));
    setState((){
      future: fetchPost();
    });
    return null;
  }
  
  Future<List<Cuenta>> fetchPost() async {
    var url = Uri.parse(ip);
    final response = await http.get(
      url,
      headers: {"token": token},
    );

    if (response.statusCode == 200) {
      // Si la llamada al servidor fue exitosa, analiza el JSON
      final jsonItems = json.decode(response.body).cast<Map<String, dynamic>>();

      List<Cuenta> accountsList = jsonItems.map<Cuenta>((json) {
        return Cuenta.fromJson(json);
      }).toList();

      return accountsList;
    } else {
      // Si la llamada no fue exitosa, lanza un error.
      throw Exception('Failed to load post');
    }
  }
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de cuentas'),
      ),
      body: FutureBuilder<List<Cuenta>>(
        future: fetchPost(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());

          return RefreshIndicator(
            child: ListView(
              
            children: snapshot.data
                .map(
                  (user) => ListTile(
                    title: Text(user.platform.toUpperCase()),
                    trailing: new Column(
                      children: <Widget>[
                        new Container(
                          child: new IconButton(
                            icon: new Icon(Icons.delete),
                            onPressed: () {
                              var url = Uri.parse(
                                  ip+"/delete_account");
                              Map datos = {"id": user.id};
                              http.delete(url,
                                  headers: {
                                    "Content-type": "application/json",
                                    "token": token
                                  },
                                  body: json.encode(datos));
                            },
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      _editClient(context, user);
                    },
                    isThreeLine: true,
                    subtitle: Text(
                        user.account + '\n' + user.password + '\n' + user.note),
                    leading: CircleAvatar(
                      backgroundColor: Colors.grey,
                      child: Text(user.platform[0].toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          )),
                    ),
                  ),
                )
                .toList(),
            ),
            onRefresh: refreshList
          );
        },
      ),
    );
  }
}

