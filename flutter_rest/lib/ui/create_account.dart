import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_rest/ui/list_accounts.dart';

import 'package:http/http.dart' as http;

var token =
"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiSm9zdWUiLCJleHAiOjE2MjU3NzU5ODN9.q8N6pjQNwQ1J4L757wyH62trZ-oi2REFKuqTUGMEADI";
var ip ="http://192.168.1.11";


class CuentaScreen extends StatefulWidget {
  final Cuenta cuenta;
  CuentaScreen(this.cuenta);
  @override
  _CuentaScreenState createState() => _CuentaScreenState();
}


class _CuentaScreenState extends State<CuentaScreen> {

  // Obteniendo informacion con los controller
  TextEditingController _accountController;
  TextEditingController _noteController;
  TextEditingController _passwordController;
  TextEditingController _platformController;

  //Vamos a juntar los Controller

  @override
  void initState() {
    super.initState();
    _accountController = new TextEditingController(text: widget.cuenta.account);
    _noteController = new TextEditingController(text: widget.cuenta.note);
    _passwordController = new TextEditingController(text: widget.cuenta.password);
    _platformController = new TextEditingController(text: widget.cuenta.platform);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('NUEVA CUENTA'),
        backgroundColor: Colors.blueAccent,
      ),
        body: Container(
          height: 570.0,
          padding: const EdgeInsets.all(20.0),
          child: Card(
            child: Center(
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _accountController,
                    style: TextStyle(fontSize: 20.0, color: Colors.grey),
                    decoration: InputDecoration(icon: Icon(Icons.person),
                        labelText: 'Cuenta'),
                  ),
                  Padding(padding: EdgeInsets.only(top : 8.0),),
                  Divider(),
                  //JOBS
                  TextField(
                    controller: _noteController,
                    style: TextStyle(fontSize: 20.0, color: Colors.grey),
                    decoration: InputDecoration(icon: Icon(Icons.work),
                        labelText: 'Nota'),
                  ),
                  Padding(padding: EdgeInsets.only(top : 8.0),),
                  Divider(),

                  //ADDRESS
                  TextField(
                    controller: _passwordController,
                    style: TextStyle(fontSize: 20.0, color: Colors.grey),
                    decoration: InputDecoration(icon: Icon(Icons.place),
                        labelText: 'Contraseña'),
                  ),
                  Padding(padding: EdgeInsets.only(top : 8.0),),
                  Divider(),

                  //ADDRESS
                  TextField(
                    controller: _platformController,
                    style: TextStyle(fontSize: 20.0, color: Colors.grey),
                    decoration: InputDecoration(icon: Icon(Icons.place),
                        labelText: 'Plataforma'),
                  ),
                  Padding(padding: EdgeInsets.only(top : 8.0),),
                  Divider(),

                  FlatButton(onPressed: (){
                    if(widget.cuenta.id != null){
                      update(widget.cuenta.id, _accountController, _noteController, _passwordController, _platformController);
                      Navigator.pop(context);
                    }else{
                      send(_accountController,_noteController,_passwordController,_platformController);
                      Navigator.pop(context);
                    }
                  },
                      child: (widget.cuenta.id != null) ? Text('Actualizar'):Text('Agregar')),
                ],
              ),
            ),

          ),
        ),
    );
  }
}

void send(_accountController,_noteController,_passwordController,_platformController) async {
  var url = Uri.parse(ip+'/create_account');
  Map datos = {		
    "account" : _accountController.text,
    "password" : _passwordController.text,
    "platform" : _platformController.text,
    "note" : _noteController.text	
  };
  http.post(url,
    headers: {"Content-type": "application/json",
              "token":token
              },
    body: json.encode(datos));
}

void update(id,_accountController,_noteController,_passwordController,_platformController) async {
  var url = Uri.parse(ip+'/update_account');
  Map datos = {
    "id":id,
    "account" : _accountController.text,
    "password" : _passwordController.text,
    "platform" : _platformController.text,
    "note" : _noteController.text	
  };
  http.put(url,
    headers: {"Content-type": "application/json",
              "token":token
              },
    body: json.encode(datos));
}